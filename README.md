# README #

An example project to test [SPH0645LM4H - Adafruit I2S MEMS Microphone Breakout](https://www.adafruit.com/product/3421) with [Arduino DUE](https://www.arduino.cc/en/Main/ArduinoBoardDue) using Atmel Studio 7.

This started from the example project called "Unit Test for the SAM SSC Driver - Arduino Due/X".


### LICENSE ###
* TBD

